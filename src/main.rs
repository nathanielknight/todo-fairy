use chrono;
use dirs;
use git2;

fn main() -> Result<(), ErrMsg> {
    let mut repo = todo_repo()?;
    if should_commit(&repo)? {
        format_git2_result(commit(&mut repo))?;
    }
    Ok(())
}

/// What to print when an unrecoverable error happens.
type ErrMsg = String;

/// Unwrap the error message in a `git2` result's error.
fn format_git2_result<T>(r: Result<T, git2::Error>) -> Result<T, ErrMsg> {
    r.map_err(|e| e.message().to_owned())
}

/// Get the path of the todo repository.
///
/// This should be a directory called 'todo' in the user's home directory.
fn todo_repo_path() -> Result<std::path::PathBuf, ErrMsg> {
    let mut homedir = dirs::home_dir().ok_or("Can't find your home directory".to_owned())?;
    let todo_path = std::path::PathBuf::from("todo");
    homedir.push(&todo_path);
    Ok(homedir)
}

fn todo_repo() -> Result<git2::Repository, ErrMsg> {
    let repo_path = todo_repo_path()?;
    format_git2_result(git2::Repository::open(&repo_path))
}

fn commit_msg() -> String {
    let today = chrono::Local::today();
    let display_date = today.format("%A %F");
    format!("End of {}", display_date)
}

/// Should a new commit be made.
///
/// New commits should be made on weekdays when there are uncommitted items in
/// the to-do repository.
fn should_commit(repo: &git2::Repository) -> Result<bool, ErrMsg> {
    fn is_weekday() -> bool {
        use chrono::{Datelike, Local, Weekday};
        let today = Local::today();
        let weekday = today.weekday();
        weekday != Weekday::Sun && weekday != Weekday::Sat
    }

    fn has_updates(repo: &git2::Repository) -> Result<bool, ErrMsg> {
        let statuses = format_git2_result(repo.statuses(None))?;
        Ok(!statuses.is_empty())
    }

    Ok(has_updates(&repo)? && is_weekday())
}

/// Commit all of the items in the todo repository (as `git commit`).
fn commit(repo: &mut git2::Repository) -> Result<(), git2::Error> {
    let mut index = repo.index()?;
    index.add_all(&["*"], git2::IndexAddOption::DEFAULT, None)?;
    let commit_tree_oid = index.write_tree()?;
    let commit_tree = repo.find_tree(commit_tree_oid)?;

    // Commit params
    let signature = git2::Signature::now("The Todo Fairy", "nathaniel.ep@gmail.com")?;
    let msg = commit_msg();
    let head = repo.head()?.peel_to_commit()?;

    repo.commit(
        Some("HEAD"),
        &signature,
        &signature,
        &msg,
        &commit_tree,
        &[&head],
    )?;

    index.update_all(&["*"], None)?;
    index.write()?;

    Ok(())
}
